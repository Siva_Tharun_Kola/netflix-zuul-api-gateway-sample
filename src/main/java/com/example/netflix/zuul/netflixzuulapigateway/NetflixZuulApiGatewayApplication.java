package com.example.netflix.zuul.netflixzuulapigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class NetflixZuulApiGatewayApplication {

  public static void main(String[] args) {
    SpringApplication.run(NetflixZuulApiGatewayApplication.class, args);
  }

}
