package com.example.netflix.zuul.netflixzuulapigateway.controller;

import org.springframework.boot.actuate.health.Status;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ZuulApiGatewayController {

  @GetMapping("/status")
  public Status getStatus() {
    return Status.UP;
  }

}
